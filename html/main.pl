#!/usr/bin/perl
use DBI;
use Time::HiRes qw(gettimeofday tv_interval);

#	 mysql -uroot -proot -h 192.168.122.74 -P 3306
my $myConnection = DBI->connect("DBI:mysql:movielens_no_ratings:db:3306", "user", "pass");
my $result = $myConnection->prepare("SELECT * FROM genres");
$startTime = [gettimeofday];
$result->execute();
#my $dump = $result->dump_results();
print "Content-type: text/plain\n\n";
$elapsed = tv_interval ($startTime);
while (my @row = $result->fetchrow_array) {
    print "$row[0]\t $row[1]\n";
}
#print $dump;
print $elapsed;

$result->finish();
$myConnection->disconnect();



